<?php 
    require_once ("./php/connection.php");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!--css-->
    <link rel="stylesheet" href="css/Add.css">
    <!--css end-->
    <!-- bootstrap-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <!-- bootstrap end-->
</head>
<body>
    
   
    <form action="productList.php" method="post">

        <!--navbar-->
        <nav class="navbar navbar-light bg-light">
            <a class="navbar-brand">product add</a>
         
            
        </nav>
    <!--navbar end--> 
     
   
    <!--inputs-->
        <div class="inputs">
            <div>
                <label for="">SKU</label>
                <input name="sku" class="inputValue" type="text">
            </div>
            <div>
                <label for="">Name</label>
                <input name="names" class="inputValue" type="text">
            </div>
            <div>
                <label for="">Price</label>
                <input name="price" class="inputValue" type="text">
            </div>
        </div> 
        <!--inputs end-->

        <!--type switcher-->
        <div>
            <div class="btn-group type-swither">
                <label class="typeSwitcher-label" for="">type switcher  </label>
                <button type="button" class="btn btn-outline-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Action
                </button>
                <div class="dropdown-menu">
                <a class="dropdown-item" onclick="openForm1()" href="#" >size</a>
                <a class="dropdown-item" onclick="openForm2()" href="#">height X width X length</a>
                <a class="dropdown-item" onclick="openForm3()" href="#">wieght</a>
                
            </div>
        </div>
        <!--type switcher end-->
        <!--forms-->
        <div id="myForm1" class="border border-dark first-form form-group">
            <h3 name="TYPE">size</h3>
            <input type="text" class="inputValue" name="size">
            <p>Lorem ipsum dolor sit amet, consectetur a
                elit, sed do eiusmod tempor incididunt 
                ut labore et dolore magna aliqua.</p>
            <button onclick="closeForm1()">close</button>    
        </div>

        <div id="myForm2" class="border border-dark first-form form-group">
            <h3>height</h3>
            <input type="text" class="inputValue" name="height">
            <h3>width</h3>
            <input type="text" class="inputValue" name="width">
            <h3>length</h3>
            <input type="text" class="inputValue" name="length">

            <p>Lorem ipsum dolor sit amet, consectetur a
                elit, sed do eiusmod tempor incididunt 
                ut labore et dolore magna aliqua.</p>
                <button onclick="closeForm2()">close</button>  
        </div>

        <div id="myForm3" class="border border-dark first-form form-group">
            <h2>weight</h2>
            <input type="text" class="inputValue" name="weight">
            <p>Lorem ipsum dolor sit amet, consectetur a
                elit, sed do eiusmod tempor incididunt 
                ut labore et dolore magna aliqua.</p>
            <button onclick="closeForm3()">close</button>  
        </div>
        <!--forms end-->
        
        <form class="form-inline">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="save" onclick="">
                save
                </button>
            </form>
        
    </form>

    <script src="index.js"></script>
   
</body>
</html>


