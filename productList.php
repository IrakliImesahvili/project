<?php 
    require_once ("./php/connection.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/List.css">
    <!-- bootstrap-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
    <!-- bootstrap end-->
</head>
<body>
     <!--navbar-->
     <nav class="navbar navbar-light bg-light">
            <a class="navbar-brand">product List</a>
            <form class="form-inline">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="save">
                apply
            </button>
            </form>
        </nav>
    <!--navbar end--> 


    <!--table-->

    <div class="d-flex table-data">
        <table class="table table-striped table-dark">
            <thead>
                <tr>
                    <td>SKU</td>
                    <td>NAMES</td>
                    <td>PRICE</td>    
                    
                    <td>SIZE</td>        

                </tr>
            </thead>
            <tbody>
                <?php 
                    if(isset($_POST['save'])){
                        $result = getData();

                        if($result){
                            while($row = mysqli_fetch_assoc($result)){?>
                                <tr>
                                    <td class="td1"><?php echo $row['SKU']; ?></td>
                                    <td><?php echo $row['NAMES']; ?></td>
                                    <td><?php echo $row['PRICE']; ?></td>
                                  
                                    <td><?php echo $row['SIZE']; ?></td>

                                </tr>


                                <?php
                                }
                            }
                        }
                    
                    ?>
            </tbody>

        </table>
    </div> 


    <!--table end-->
    <!-- form-->
            



    <!-- form ned-->
     <script src="index.js"></script>
</body>
</html>